xfce4-verve-plugin (2.0.0-1) unstable; urgency=medium

  * Moved the package to git on salsa.debian.org
  * Updated the maintainer address to debian-xfce@lists.debian.org
  * d/gbp.conf added, following DEP-14
  * New upstream version 2.0.0
  * run wrap-and-sort
  * d/control: add b-d on libglib, libxfce4panel-2.0 and libxfce4ui-2
  * update standards version to 4.1.4
  * d/compat bumped to 10
  * d/control: update debhelper b-d to 10 for compat 10
  * d/rules: drop --parallel and override autoreconf
  * d/control: drop autotools-dev b-d

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 06 May 2018 16:23:55 +0200

xfce4-verve-plugin (1.1.0-1) unstable; urgency=medium

  [ Mateusz Łukasik ]
  * New upstream release.
  * debian/patches:
    - 01_fix-format-string.patch dropped, included upstream.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 18 Oct 2015 14:39:48 +0200

xfce4-verve-plugin (1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    - 01_configure-call-xdt-feature-debug dropped, included upstream.
    - 01_fix-format-string added, fix format string issue.
  * debian/control:
    - replace libxfcegui4 build-dep by libxfce4ui.
  * debian/rules:
    - remove .la files from the package.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 06 May 2015 22:29:59 +0200

xfce4-verve-plugin (1.0.0-3) unstable; urgency=low

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Yves-Alexis Perez ]
  * debian/control:
    - update standards version to 3.9.6.
    - add build-dep on automake
  * debian/rules:
    - call xdt-autogen before configure to regenerate config.{guess,sub} and
      gain support for new architectures.                       closes: #765279 
  * debian/patches:
    - 01_configure-call-xdt-feature-debug added, replace BM_DEBUG_SUPPORT() by
      XDT_FEATURE_DEBUG() in configure.ac, fix FTBFS when combined with
      xdt-autogen.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 15 Oct 2014 22:25:34 +0200

xfce4-verve-plugin (1.0.0-2) unstable; urgency=low

  * debian/rules:
    - use dh tiny rules.
    - use dpkg/debhelper hardening support.
    - enable all hardening flags.
    - enable parallel build.
  * debian/compat bumped to 9.
  * debian/control:
    - update debhelper build-dep to 9.
    - drop build-dep on hardening-includes.
    - update standards version to 3.9.4. 
    - drop versions in Xfce build-deps, since stable has 4.8 now. 

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 26 May 2013 13:54:57 +0200

xfce4-verve-plugin (1.0.0-1) unstable; urgency=low

  [ Evgeni Golov ]
  * Fix Vcs-* fields, they were missing 'trunk' in the path.

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/watch edited to track Xfce archive reorganisation.
  * debian/control:
    - switch to xfce section.
    - add build-dep on libxfcegui4-dev. 
    - add build-dep on xfce4-dev-tools and libtool
    - add build-dep on hardening-includes.
    - update standards version to 3.9.2.
    - update debhelper build-dep to 7.
    - drop cdbs build-dep.
  * debian/rules:
    - run xdt-autogen after applying patch touching configure.in/Makefile.am
    - pick {C,LD}FLAGS from dpkg-buildflags.
    - add -O1, -z,defs and --as-needed to LDFLAGS.
    - add hardening flags to {C,LD}FLAGS.
    - switch to dh7.
  * Switch to 3.0 (quilt) source format.
  * debian/compat bumped to 7.

  [ Lionel Le Folgoc ]
  * debian/control:
    - change libexo-0.3-dev b-dep to libexo-1-dev.
    - add myself to Uploaders.
    - remove Simon and Emanuele from uploaders, thanks to them.
    - bump xfce4-panel-dev b-dep to (>= 4.8.0).
    - do not refer to Xfce 4.4 in the descriptions.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 20 Apr 2011 10:17:21 +0200

xfce4-verve-plugin (0.3.6-1) unstable; urgency=low

  * New upstream release.
    - fix crash during initialization of completion system. closes: #470726
  * debian/control:
    - update standards version to 3.8.0.
    - remove Martin and Rudy from Uploaders:, thanks to them!
    - add build-dep on intltool.
  * debian/patches:
    - 01_typo-fr.po dropped, merged upstream.
  * debian/watch:
    - update url to match tarball renaming.

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 28 Feb 2009 20:38:09 +0100

xfce4-verve-plugin (0.3.5-3) unstable; urgency=low

  * debian/patches: 01_typo-fr.po added, thanks Cyril Brulebois.closes: #435477
  * debian/control:
    - updated standard versions.
    - updated my email address.
  * debian/copyright: update, add © sign and dates.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 27 Apr 2008 19:22:11 +0200

xfce4-verve-plugin (0.3.5-2) unstable; urgency=low

  * Add Vcs-* headers to debian/control
  * Add dependency on new exo-utils for exo-open

 -- Simon Huggins <huggie@earth.li>  Mon, 28 Jan 2008 11:34:36 +0000

xfce4-verve-plugin (0.3.5-1) unstable; urgency=low

  (Yves-Alexis Perez)
   * New upstream release. 
  (Simon Huggins)
   * Build against latest and greatest xfce.

 -- Yves-Alexis Perez <corsac@corsac.net>  Mon, 16 Apr 2007 18:55:33 +0100

xfce4-verve-plugin (0.3.4-1) unstable; urgency=low

  (Yves-Alexis Perez)
   * New upstream release
   * Updated build-deps to 4.3.90.2 (Xfce 4.4 Beta2).
   * Updated standards version to 3.7.2.
  (Simon Huggins)
   * Add manpage for verve-focus.

 -- Yves-Alexis Perez <corsac@corsac.net>  Sat, 05 Aug 2006 17:18:44 +0100

xfce4-verve-plugin (0.3.0-1) unstable; urgency=low

  * Initial release Closes: #363466

 -- Yves-Alexis Perez <corsac@corsac.net>  Sun, 14 May 2006 14:56:46 +0100

